#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''

Version: 180402
This scenarium may enable/disable AndroidAuto and CarPlay

Name of this script should be exactly the same as in scenaruim URL but with '.py' extension

URL  -  scm:SCEN_ECRI_GENERIQUE2#SCEN_ECRI_GENERIQUE2_<eid>.xml

'run' procedure will be executed by pyren script
 
'''

import os
import sys
import re
import time

import mod_globals
import mod_utils
import mod_ecu
from   mod_utils     import pyren_encode
from   mod_utils     import clearScreen

import xml.dom.minidom

def run( elm, ecu, command, data ):
  '''
  MAIN function of scenarium
  
  Parameters:
      elm     - refernce to adapter class
      ecu     - reference to ecu class
      command - refernce to the command this scenarium belongs to
      data    - name of xml file with parameters from scenarium URL
  '''
  
  clearScreen()
  header =  '['+command.codeMR+'] '+command.label
  
  #
  #     Important information
  #
  clearScreen()
  print pyren_encode(header)
  print
  print 'BU SENARYO AndroidAuto and CarPlay UYGULAMALARINI ACIP KAPATIR'
  print
  print '*'*50
  
  #
  #     check if this ECU is supported
  #
  eid = data[-9:-4]
  if eid not in ['11300']:
    print '\n\nBU ECU YAPILANDIRILMAMIS !!!!\n\n'
    ch = raw_input('CIKMAK iCiN ENTER YAPIN')
    return
  
  #
  #     read current value
  #
  print 'KULLANICI DEGERLERi OKUNUYOR'
  rsp = elm.request("222130",positive='622130', cache=False)
  rsp = rsp.replace(' ','')[:20]
  print "Done:", rsp
  if not rsp.startswith('622130'):
    print 'GONDERiLEN KOMUTTA YANLISLIK VAR !!!'
    ch = raw_input('CIKMAK iCiN ENTER YAPIN')
    return
  hexVal = int(rsp[8:9],16)
  print '*'*50
  if hexVal & 0x2:
    print 'AndroidAuto   : ACIK'
  else:
    print 'AndroidAuto   : KAPALI'
  if hexVal & 0x4:
    print 'CarPlay       : ACIK'
  else:
    print 'CarPlay       : KAPALI'
  print '*'*50
  
  #
  #     changing value
  #
  ch = raw_input ('ACMAK iCiN < on > KAPATMAK iCiN < off > CIKMAK iCiN < quit > YAZIN :')
  if ch.lower () != 'on' and ch.lower () != 'off': return

  if ch.lower () == 'off':
    print 'ESLESME KAPALI !!!'
    hexVal = hexVal & 0x9
  elif ch.lower () == 'on':
    print 'ESLESME ACIK !!!'
    hexVal = hexVal | 0x6
  newcmd = '2E2130'+rsp[6:8]+hex(hexVal)[-1:].upper()+rsp[9:]
  
  #
  #     writing value
  #
  print 'YENi :',newcmd
  print 'DEGiSiKLiKLER OKUNUYOR'
  ch = raw_input ('ACMAK iCiN < on > KAPATMAK iCiN < off > CIKMAK iCiN < quit > YAZIN :')
  if ch.lower () != 'yes': return
  rsp = elm.request(newcmd, positive='6E2130', cache=False)
  if not rsp.upper().replace(' ','').startswith('6E2130'):
    print 'RSP :',rsp
    print 'HATALI GiRiS!!!'
    ch = raw_input('CIKMAK iCiN ENTER YAPIN')
    return

  #
  #     wait a bit
  #
  time.sleep(2)
  
  #
  #     read new value
  #
  print 'YENi DEGERLER OKUNUYOR'
  rsp = elm.request ("222130", positive='622130', cache=False)
  rsp = rsp.replace (' ', '')[:20]
  print "TAMAMLANDI :", rsp
  if not rsp.startswith ('622130'):
    print 'HATALI GiRiS !!!'
    ch = raw_input ('CIKMAK iCiN ENTER YAPIN')
    return
  hexVal = int (rsp[8:9], 16)
  print '*' * 50
  if hexVal & 0x2:
    print 'AndroidAuto   : ACIK'
  else:
    print 'AndroidAuto   : KAPALI'
  if hexVal & 0x4:
    print 'CarPlay       : ACIK'
  else:
    print 'CarPlay       : KAPALI'
  print '*' * 50

  print '\n\n\t TAMAMLANDI'
  print '\n\n AYGITINIZI MANUEL OLARAK RESETLEYiN '
  print ' AYGITINIZIN GUC BUTONUNA UZUN SURE BASILI TUTUN\n\n'
  ch = raw_input('DEVAM ETMEK iCiN ENTER A BASIN')

